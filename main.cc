#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <iostream>
#include <assert.h>


#define LOOP_QUEUE_SIZE 16

typedef struct loop_queue_t
{
  uint8_t queue[LOOP_QUEUE_SIZE];
  uint8_t *start_ptr;
  uint8_t *end_ptr;
  int8_t carry;
} loop_queue_t;


char warning_msg[100] = "warning message%s";

#define SIZE 4
#if SIZE!=4
// sprintf(warning_msg, "warning message%s", "what");
// __warning__(EAGAIN, warning_msg,sizeof(queue));
#warning (EAGAIN, "THE LENGTH IS NOT MATCHED",); //warning_msg,sizeof(queue));
#endif

// there are 2 situations start_ptr == end_ptr
// when queue_length is 0 or queue_equals to sizeof (queue->queue)
// I do not know how to deal with them now
uint32_t queue_length (const loop_queue_t *queue)
{
  // take care, diff must be signed integer
  const int32_t diff = queue->end_ptr - queue->start_ptr;
  // const uint32_t queue_length = (diff >= 0) ? diff : (sizeof (queue->queue) + diff);

  if (0 == diff)
  {
    return queue->carry * sizeof (queue->queue);
  }
  else if (0 < diff)
  {
    return diff;
  }
  else if (0 > diff)
  {
    return diff + sizeof (queue->queue);
  }
  else
  {
    assert (false);
  }
  // return queue_length;
}


// only the start_ptr or end_ptr of the same queue can transfer into this function
static void queue_ptr_move (loop_queue_t *queue, uint8_t **ptr, uint32_t delta)
{
  // TODO: rafaellee.img@gmail.com
  // 20190526,
  uint32_t start_ptr_offset = (uint8_t *)queue->start_ptr - (uint8_t *)queue;
  uint32_t end_ptr_offset = (uint8_t *)queue->end_ptr - (uint8_t *)queue;
  int32_t is_start_ptr = 0;

  // here is a trick, use distance between pointer of ptr and distance of queue
  // to tell if the ptr is start_ptr or end_ptr
  if ((uint8_t *) (*ptr) - (uint8_t *)queue == start_ptr_offset)
  {
    // std::cout << "ptr - queue == start_ptr_offset" << std::endl;
    is_start_ptr = 1;
  }
  else if ((uint8_t *) (*ptr) - (uint8_t *)queue == end_ptr_offset)
  {
    // std::cout << "ptr - queue == end_ptr_offset" << std::endl;
    is_start_ptr = 0;
  }
  else
  {
    // program should not run to here
    assert (false);
  }
  uint8_t *queue_spacial_end = sizeof (queue->queue) + queue->queue;

  *ptr = *ptr + delta;
  if (delta > 0)
  {
    // bigger than or equal
    //  [0,1,2,3,4] index = 5; ptr can be 5
    while (*ptr >= queue_spacial_end)
    {
      1 == is_start_ptr ? queue->carry = 0 : queue->carry = 1;
      *ptr -= sizeof (queue->queue);
    }
  }
  else // delta < 0
  {
    while (*ptr < queue->queue) // spacial queue start
    {
      // 1 == is_start_ptr ? queue->carry -= 1 : queue->carry += 1;
      1 == is_start_ptr ? queue->carry = 1 : queue->carry = 0;
      *ptr += sizeof (queue->queue);
    }
  }
  // std::cout << "queue->carry = " << (int)queue->carry << std::endl;
}

// nothing to say
uint32_t queue_init (loop_queue_t *queue)
{
  queue->start_ptr = queue->queue;
  queue->end_ptr = queue->queue;
  queue->carry = 0;

  std::cout << "in queue_init(), sizeof(queue->queue) = " << sizeof (queue->queue) << std::endl;
  std::cout << "sizeof(queue) = " << sizeof (*queue) << std::endl;
  std::cout << "queue is " << (uint32_t *) (void *)queue << std::endl;
  memset (queue->queue, 0, sizeof (queue->queue));
  return 0;
}

uint32_t queue_available_length (loop_queue_t *queue)
{
  return ((sizeof (queue->queue)) - queue_length (queue));
}

uint32_t queue_push_back (loop_queue_t *queue,  const uint8_t data)
{
  // uint32_t data_length = queue_length (queue);
  // uint32_t available_length = (sizeof (queue->queue)) - data_length;

  uint32_t available_length = queue_available_length (queue);
  if (1 <= available_length)
  {
    * (queue->end_ptr) = data;
    queue_ptr_move (queue, & (queue->end_ptr), 1);
    return 0;
  }
  else
  {
    return 0;
  }
}


// no overwrite to queue_dst
uint32_t queue_push_back_ex (loop_queue_t *queue,  uint8_t *data_src, const uint32_t length)
{
  uint8_t *queue_end = queue->queue + sizeof (queue->queue);

  uint32_t available_length = queue_available_length (queue);

  // if queue_length > queue_available_length
  if (length > available_length)
  {
    // shrink the length to copy, recursive function, take care
    return queue_push_back_ex (queue,  data_src, available_length);
  }
  // else available length > length
  else
  {
    // overflow
    if (queue->end_ptr + length > queue_end)
    {
      // copy to end of queue
      uint32_t length_to_copy = queue_end - queue->end_ptr;
      memcpy (queue->end_ptr, data_src, length_to_copy);

      // copy from start of queue
      queue->end_ptr = queue->queue;
      memcpy (queue->end_ptr, data_src + length_to_copy, length - length_to_copy);
      queue_ptr_move (queue, & (queue->end_ptr), length);
      return length;
    }
    else // copy directly, no overflow
    {
      memcpy (queue->end_ptr, data_src, length);
      queue_ptr_move (queue, & (queue->end_ptr), length);
      return length;
    }
  }
}


// transfer maximum amount of data to queue_dst, from the start of queue_src
uint32_t queue_transfer_ex (loop_queue_t *queue_dst, loop_queue_t *queue_src)
{
  uint32_t length_to_copy = queue_length (queue_src);
  uint32_t sum = 0;
  if (queue_src->end_ptr >= queue_src->start_ptr)
  {
    // queue_dst->end_ptr is modified in queue_push_back_ex()
    sum = queue_push_back_ex (queue_dst,  queue_src->start_ptr, length_to_copy);
    // queue_str->start_ptr =
    queue_ptr_move (queue_src, & (queue_src->start_ptr), sum);
    return sum;
  }
  else //  addr_low end_ptr ... start_ptr, addr high
  {
    // queue_dst->end_ptr is modified in queue_push_back_ex()
    //                                                                              spacial end of queue_est        -  queue start
    sum = queue_push_back_ex (queue_dst,  queue_src->start_ptr, queue_src->queue + sizeof (queue_src->queue) - queue_src->start_ptr);
    // assert_param (queue_src queue_dst->start_ptr);
    sum += queue_push_back_ex (queue_dst,  queue_src->queue, queue_src->end_ptr - queue_src->queue);
    queue_ptr_move (queue_src, & (queue_src->start_ptr), sum);
    return sum;
  }
}


// debug test queue
void queue_print (loop_queue_t *queue, char *queue_name)
{
  // uint32_t length = queue_length (queue);
  uint32_t size = sizeof (queue->queue);
  // std::cout << "queue_length(&qb) = " << queue_length (&qb) << std::endl;

  printf ("queue_length(%s) = %2d, data = \n", queue_name, queue_length (queue));
  char buffer_0 [81];
  char buffer_1 [81];
  memset (buffer_0, ' ', sizeof (buffer_0));
  buffer_0[sizeof (buffer_0)] = '\0';
  memset (buffer_1, ' ', sizeof (buffer_1));
  buffer_1[sizeof (buffer_1)] = '\0';

  int index_start = queue->start_ptr - queue->queue;
  buffer_0[6 * index_start + 1] = '|';
  buffer_1[6 * index_start + 1] = 'S';

  int index_end = queue->end_ptr - queue->queue;
  buffer_0[6 * index_end + 3] = '|';
  buffer_1[6 * index_end + 3] = 'E';
  printf (buffer_0);
  printf ("\n");
  printf (buffer_1);
  printf ("\n");


  for (uint32_t i = 0; i < size ; i++)
  {
    printf ("0x%02X, ", * (queue->queue + i));
  }
  printf ("\n");
  fflush (stdout);
}


int main (void)
{

  uint32_t a = 12;
  uint32_t *pa = nullptr;

  std::cout << pa << std::endl;
  // std::cout << *pa << std::endl;
  std::cout << &a << std::endl;

  pa = &a;
  a =  3;
  std::cout << pa << std::endl;
  std::cout << *pa << std::endl;
  std::cout << &a << std::endl;

  *pa =  5555;
  std::cout << pa << std::endl;
  std::cout << *pa << std::endl;
  std::cout << &a << std::endl;

  loop_queue_t qa, qb;
  queue_init (&qa);
  queue_init (&qb);
  for (uint8_t i = 0; i < 12; i++)
  {
    queue_push_back (&qa, i + 'a');
  }
  assert (queue_length (&qa) == 12 && "queue_length (&qa) != 12");
  assert (queue_available_length (&qa) == 4);


  for (uint8_t i = 0; i < 4; i++)
  {
    queue_push_back (&qb, '0' + i % 10);
  }
  assert (queue_length (&qb) == 4 && "queue_length (&qa) != 4");
  assert (queue_available_length (&qb) == 12);
  // queue_push_back (&qb, ' ');


  // ****************************************

  // std::cout << "queue_length(&qb) = " << queue_length (&qb) << std::endl;

  // std::cout << "queue_length(&qa) = " << queue_length (&qa) << std::endl;
  queue_print (&qa, "qa");

  // queue_transfer_ex (&qb, &qa);
  // std::cout << "queue_length(&qb) = " << queue_length (&qb) << std::endl;
  queue_print (&qb, "qb");

  // simulate to remove 5 elements from qa head
  memset (qa.start_ptr, 0, 5);
  qa.start_ptr = qa.start_ptr + 5;
  assert (queue_length (&qa) == 7  && "queue_length (&qa) != 7");
  assert (queue_available_length (&qa) == 9);

  queue_transfer_ex (&qa, &qb);
  assert (queue_length (&qa) == 11 && "queue_length (&qa) != 11");
  assert (queue_available_length (&qa) == 5);

  // std::cout << "queue_length(&qa) = " << queue_length (&qa) << std::endl;
  queue_print (&qa, "qa");

  for (uint8_t i = 0; i < 4; i++)
  {
    queue_push_back (&qb, '0' + i % 10);
  }
  // std::cout << "queue_length(&qb) = " << queue_length (&qb) << std::endl;
  queue_print (&qb, "qb");

  queue_transfer_ex (&qa, &qb);
  queue_print (&qa, "qa");

  assert (queue_length (&qa) == 15 && "queue_length (&qa) != 15");
  assert (queue_available_length (&qa) == 1);

  // test queue_push_back
  queue_push_back (&qb, '*');


  queue_transfer_ex (&qa, &qb);
  queue_print (&qa, "qa");
  assert (queue_length (&qa) == 16 && "queue_length (&qa) != 16");
  assert (queue_available_length (&qa) == 0);

  queue_print (&qb, "qb");
  assert (queue_length (&qb) == 0 && "queue_length (&qb) != 0");
  assert (queue_available_length (&qb) == 16);

  return 0;
}


